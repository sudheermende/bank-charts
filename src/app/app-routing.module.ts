import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:"dashboard",
    loadChildren:"./featureModules/dashboard/dashboard.module#DashboardModule",
    data:{title:"Dashboard"}
  },
  {
    path:"",
    redirectTo:"dashboard",
    pathMatch:"full"
  },
  {
    path:"**",
    redirectTo:"dashboard",
    pathMatch:"full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
