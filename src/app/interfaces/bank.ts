export interface Bank {
    rank : number;
    name : string;
    location : string;
    totalAssets : number;
    marketCapitalization : number
}
