import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ng2GoogleChartsModule} from 'ng2googlecharts';

import { MatTableModule} from '@angular/material/table';
import { MatButtonModule} from '@angular/material/button';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule} from '@angular/material/sort';
import { MatInputModule} from '@angular/material/input';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports:[
    Ng2GoogleChartsModule,
    MatTableModule,MatButtonModule,
    MatProgressBarModule,MatPaginatorModule,
    MatSortModule,MatInputModule,MatFormFieldModule,
    MatTooltipModule
  ]
})
export class SharingModule { }
