import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Bank } from '../../../interfaces/bank';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { style } from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private dataService:DataService) { }

  public bankData : Bank[];
  public displayedColumns: string[] = ['rank', 'name', 'location', 'totalAssets','marketCapitalization'];
  public lableNames = {
    rank : "Rank",name:"Bank Name",
    location : "Headquarters location",
    totalAssets :"Total assets (billions of US$)",
    marketCapitalization:"Market capitalization (billions of US$)"
  }
  public dataSource :MatTableDataSource<Bank>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public isLoading = true;
  public isValidData = false;
  public isChartLoaded = false;
  
  public midAmount =750;
  public highAmount = 1500;
  public pageOptions = [20,50,100,150,200];

  public chartOptions = 
  {'title':"Bank's Assets",
    legend:{
      position:'none'
    }, 
    animation:{ 
      duration: 1000, easing: 'out', startup: true 
    },
    height:600
   };

  public barChartData =  {
    chartType: 'BarChart',
    dataTable: [],
    options: this.chartOptions,
  };

  ngOnInit() {
    //  Load bank data from service
    this.dataService.fetchBankData().subscribe((list:Bank[]) =>{

        this.isLoading= false
        try{
          this.bankData = list;
          this.dataSource = new MatTableDataSource<Bank>(this.bankData);
          this.prepareDataForChart();
          this.initTable();
          this.isValidData =true;
        }catch(e){
            this.isValidData =false;
        }
    })
  }
  
  prepareDataForChart(){
    // Finding 20 Banks with high assets ( Not using 'ranks')

      this.bankData.sort((a,b) => b.totalAssets - a.totalAssets);
      let topBanks = this.bankData.slice(0,20);

      let chartData:any = topBanks.map(eachBank => {

        let record = [eachBank.name,eachBank.totalAssets]
      
          if(eachBank.totalAssets < this.midAmount)
              record.push('red');
          else if(eachBank.totalAssets >= this.midAmount && eachBank.totalAssets < this.highAmount)
              record.push('orange');
          else if(eachBank.totalAssets >= this.highAmount)
              record.push('green');

              return record;
      });
        chartData.unshift(['Bank',this.lableNames.totalAssets,{role: "style"}]);
        
          this.barChartData.dataTable= chartData;
  }

  initTable(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // Filter the data
  search(search: string) {
    this.dataSource.filter = search.trim().toLowerCase();
  }

  print(){
      try{
        window.print();
      }catch(e){
          console.log("Unable To Print")
      }
  }
}
