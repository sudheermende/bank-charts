import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { HttpClientModule} from '@angular/common/http';

import { SharingModule} from '../../sharing-module/sharing-module.module';
import { DashboardComponent } from './components/dashboard.component';
import { DataService} from './services/data.service';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SharingModule,
    RouterModule.forChild([
        {
          path:"",
          children:[
            {
              path:"",
              component:DashboardComponent
            }
          ]
        }
    ])
  ],
  providers:[DataService]
})
export class DashboardModule { }
